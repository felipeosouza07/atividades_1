//comentar
/*
Faça um programa que receba quatro números inteiros,
calcule e mostre a soma desses números.
*/

//Primeiro bloco
//Inicio
namespace exercicio_1
{
    //Entrada dos dados
    //var --- let --- const
    let numero1, numero2, numero3, numero4: number;
    numero1 = 5;
    numero2 = 10;
    numero3 = 15;
    numero4 = 20;

    let resultado: number;

    //Processar os dados
    resultado = numero1 + numero2 + numero3 + numero4;

    //Saída
    console.log("O resultado da soma é: " + resultado);
    //Ou pode-se escrever
    console.log(`O resultado da soma é: ${resultado}`);

}