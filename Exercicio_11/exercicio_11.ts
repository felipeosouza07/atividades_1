//Inicio
namespace exercicio_11 {
  //Entrada de dados
  const numero = 10;

  let numQ: number;
  numQ = numero * numero;

  let numC: number;
  numC = Math.pow(numero, 3);

  let raizQ: number;
  raizQ = Math.sqrt(numero);

  let raizC: number;
  raizC = Math.cbrt(numero);

  console.log(
    `O número elevado ao quadrado: ${numQ} \n O número elevado ao cubo: ${numC} \n A raiz quadrada do número: ${raizQ} \n A raiz cúbica do número: ${raizC}`
  );
}
