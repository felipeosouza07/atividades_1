namespace exercicio_3
{
    let nota1, peso1, nota2, peso2, nota3, peso3: number;

    nota1 = 10;
    nota2 = 4;
    nota3 = 9;
    peso1 = 1;
    peso2 = 5;
    peso3 = 4;

    let resultado: number;
    resultado = (nota1 + nota2 + nota3) / peso1 + peso2 + peso3;

    console.log(`A média ponderada é: ${resultado}`);
}
